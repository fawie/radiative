# -*- coding: utf-8 -*-
import math

# in eV
T_i=1
T_f=2000

print("T_i")
print(math.log10(T_i/8.617328149e-5))

print("Delta T_i - T_f")
print(math.log10(T_i/8.617328149e-5)-math.log10(T_f/8.617328149e-5))
