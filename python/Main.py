# -*- coding: utf-8 -*-
"""
Created on Tue Oct 16 21:38:11 2018

@author: Fabian
"""
import numpy as np
import os
import matplotlib.pylab as plt
from matplotlib import rcParams
import seaborn as sns
from decimal import Decimal


#Einstellungen des Graphen
rcParams.update({'figure.autolayout': True,'font.size': 24.,'lines.linewidth':3.5,'savefig.dpi':150.})
sns.set_style("ticks", {"axes.facecolor": ".82",'grid.color': '1.'})
sns.set_context("talk",font_scale=1.2,rc={"lines.linewidth":3.5})
sns.set_palette("deep")
#plt.rc('text', usetex=True)
#font={'size': 204.7}
#plt.rc('font', family='serif',**font)
params = {'axes.labelsize': 24,'axes.titlesize':24, 'font.size': 24, 'legend.fontsize': 24, 'xtick.labelsize': 24, 'ytick.labelsize': 24}
rcParams.update(params)


def text2array(file,a=1,b=999999999):
    data = open(file, "r")
    data=data.readlines()
    data=data[a-1:b-1]
    for i in range(len(data)):
        data[i]=[elem for elem in data[i].split()]
        for j in range(len(data[i])):
          if  "*" in data[i][j] or "E" not in data[i][j] : 
            data[i][j]=0.0
          else:
            data[i][j]=float(data[i][j])

        
    data=np.matrix(data).T.tolist()
    data=[np.array(dat) for dat in data]
    
    return data

daten=text2array("charge_distribution_evolution_with.dat",1,909999)
#daten_wo=text2array("charge_distribution_evolution_without.dat",1,909999)
#daten=np.loadtxt("charge_distribution_evolution.dat",skiprows=1)

daten[2]=daten[2]*8.617328149e-5 # K to eV
daten[0]=daten[0]*1000. # s to ms

#daten_wo[0]=daten_wo[0]*1000.


xlim=[1.e-6,2.e-3]
figsize=(10.5,6.)

fig = plt.figure(figsize=figsize)
plt.xlabel("t/ms")
plt.ylabel("T/keV")
plt.grid(zorder=0)
sns.despine(offset=10)
plt.xlim(xlim)

plt.plot(daten[0],daten[2])
plt.savefig("Temperatures.png") 


fig = plt.figure(figsize=figsize)
plt.xlabel("t/ms")
plt.ylabel("%")
plt.grid(zorder=0)
sns.despine(offset=10)

n_Z0=4

plt.xlim(xlim)

#iz=[2,3,4,5,15,16,17,18]
#iz=[0,1,2,3,4,5,6,7,8,15,16,17]
iz=range(n_Z0)
palette=sns.color_palette("colorblind",len(iz))


for jz in range(len(iz)):
  plt.plot(daten[0],100.*daten[3+iz[jz]],color=palette[jz],linewidth=2.,label="z="+str(iz[jz]))
  plt.plot(daten[0],100.*daten[3+n_Z0+iz[jz]],color=palette[jz],linestyle='dashed',linewidth=2.)


lgd=plt.legend(loc='upper left', bbox_to_anchor=(1., 1.))

#plt.plot(daten[0],100.*np.average(daten[3+n_Z0:3+2*n_Z0],axis=0),color='red')
#plt.plot(daten[0],100.*np.average(daten[3+2*n_Z0:3+3*n_Z0],axis=0),color='blue')

plt.savefig("f.png", bbox_extra_artists=(lgd,), bbox_inches='tight')


fig = plt.figure(figsize=figsize)
plt.xlabel("t/ms")
plt.ylabel("Prad/Wm^3")
plt.grid(zorder=0)
sns.despine(offset=10)
plt.xlim(xlim)
#plt.ylim([3.e-33,1.e-30])
plt.yticks([])

plt.semilogy(daten[0],daten[3+3*n_Z0],color="green",linewidth=2.,label="Temporal")
plt.semilogy(daten[0],daten[4+3*n_Z0],color="green",linestyle='dashed',linewidth=2.,label="Coronal equil.")
#plt.loglog(daten_wo[0],daten_wo[3+3*n_Z0]*1e-20,color="blue", linewidth=2.,label="Without diff.")
	
#lgd=plt.legend(loc='upper left', bbox_to_anchor=(1.,1.))
plt.legend()

#plt.savefig("Radiation.png", bbox_extra_artists=(lgd,), bbox_inches='tight')
plt.savefig("Prad.png")


fig = plt.figure(figsize=figsize)
plt.xlabel("t/ms")
plt.ylabel("<Z>")
plt.grid(zorder=0)
sns.despine(offset=10)
plt.xlim(xlim)

plt.plot(daten[0],daten[6+3*n_Z0],color="green",linewidth=2.,label="Temporal.")
plt.plot(daten[0],daten[7+3*n_Z0],color="green",linestyle='dashed',linewidth=2.,label="Coronal equil.")
#plt.loglog(daten_wo[0],daten_wo[3+3*n_Z0]*1e-20,color="blue", linewidth=2.,label="Without diff.")
	
#lgd=plt.legend(loc='upper left', bbox_to_anchor=(1.,1.))
plt.legend()

#plt.savefig("Radiation.png", bbox_extra_artists=(lgd,), bbox_inches='tight')
plt.savefig("Zeff.png")

