# -*- coding: utf-8 -*-

def Poincare_R_Z(data,run,step,plot):
    fig = plt.figure(figsize=(8.*1.6,13.*1.6))
    plt.xlim([1.,2.2])
    plt.ylim([-1.3,1.1])
    plt.grid(zorder=0)
    sns.despine(offset=10)
    #plt.xticks([])
    #plt.xlim([0.5,1])
    plt.xlabel(r"$R$")
    plt.ylabel(r"$Z$")
    plt.title("$t="+str(int(data[run]["times"][1][step]))+"$")
    #plt.title("fact="+"{:.2E}".format(Decimal(data[run]["input_jorek"]["wall_resistivity_fact"]))+", t="+str(data[run]["times"][step]))
    #plt.title(labels[5]+" t="+str(np.round(prefix*np.sqrt(scic.mu_0*rho_0)*1000*100)/100)+"ms")
    plt.plot(RetData(data,run,"poincare/R-Z",step,"R"),RetData(data,run,"poincare/R-Z",step,"Z"),"ro",markersize=1)
        
    if(plot):
        print("../plots/"+run+"_poinc_R-Z_"+str(step).zfill(5)+".png")
        plt.savefig("../plots/"+run+"_poinc_R-Z_"+str(step).zfill(5)+".png") 
        plt.close()
        
	
def Poincare_PsiN_theta(data,run,step,xlim=0,plot=False):
    fig = plt.figure(figsize=(7.,6.5))
    if xlim == 0 :
       plt.xlim([0.,1.])
    else:
       plt.xlim(xlim)
    plt.ylim([-np.pi,np.pi])
    plt.grid(zorder=0)
    sns.despine(offset=10)

    plt.xlabel(r"$\Psi_N$")
    plt.ylabel(r"$\theta$")
    #plt.title("$t="+str(int(data[run]["times"][1][step]))+"$")
    #plt.title("fact="+"{:.2E}".format(Decimal(data[run]["input_jorek"]["wall_resistivity_fact"]))+", t="+str(data[run]["times"][step]))
    #plt.title(labels[5]+" t="+str(np.round(prefix*np.sqrt(scic.mu_0*rho_0)*1000*100)/100)+"ms")
    plt.plot(RetData(data,run,"poincare/rho-theta",step,"rho")**2,RetData(data,run,"poincare/rho-theta",step,"theta"),"ro",markersize=1)
        
    if(plot):
        print("../plots/"+run+"_poinc_PsiN-theta_"+str(step).zfill(5)+".png")
        plt.savefig("../plots/"+run+"_poinc_PsiN-theta_"+str(step).zfill(5)+".png") 
        plt.close() 
    else:
        plt.show()
        
def Poincare_PsiN_theta_black(data,run,step,plot=False):
    fig = plt.figure(figsize=(12,6.5),facecolor='black')
    plt.xlim([.3,1.])
    plt.ylim([-np.pi,np.pi])
    plt.grid(zorder=0)
    plt.axis('off')
    
    #plt.title("$\\texttt{fact="+"{:.2E}".format(Decimal(data[run]["input_jorek"]["wall_resistivity_fact"]))+", t="+str(data[run]["times"][step])+"}$",color="white", loc='left')
    plt.plot(RetData(data,run,"poincare/rho-theta",step,"rho")**2,RetData(data,run,"poincare/rho-theta",step,"theta"),"wo",markersize=1)
        
    if(plot):
        print("../plots/"+run+"_poinc_PsiN-theta_black_"+str(step).zfill(5)+".png")
        plt.savefig("../plots/"+run+"_poinc_PsiN-theta_black_"+str(step).zfill(5)+".png",bbox_inches='tight',facecolor='black') 
        plt.close()

        

	

