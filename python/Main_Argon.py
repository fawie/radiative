# -*- coding: utf-8 -*-
"""
Created on Tue Oct 16 21:38:11 2018

@author: Fabian
"""
import numpy as np
import os
import matplotlib.pylab as plt
from matplotlib import rcParams
import seaborn as sns
from decimal import Decimal


#Einstellungen des Graphen
rcParams.update({'figure.autolayout': True,'font.size': 24.,'lines.linewidth':3.5,'savefig.dpi':150.})
sns.set_style("ticks", {"axes.facecolor": ".82",'grid.color': '1.'})
sns.set_context("talk",font_scale=1.2,rc={"lines.linewidth":3.5})
sns.set_palette("deep")
#plt.rc('text', usetex=True)
#font={'size': 204.7}
#plt.rc('font', family='serif',**font)
params = {'axes.labelsize': 24,'axes.titlesize':24, 'font.size': 24, 'legend.fontsize': 24, 'xtick.labelsize': 24, 'ytick.labelsize': 24}
rcParams.update(params)


def text2array(file,a=1,b=999999999):
    data = open(file, "r")
    data=data.readlines()
    data=data[a-1:b-1]
    for i in range(len(data)):
        data[i]=[elem for elem in data[i].split()]
        for j in range(len(data[i])):
          if  "*" in data[i][j] or "E" not in data[i][j] : 
            data[i][j]=0.0
          else:
            data[i][j]=float(data[i][j])

        
    data=np.matrix(data).T.tolist()
    data=[np.array(dat) for dat in data]
    
    return data

daten=text2array("tungstentest-SI.dat",2,1000000)


daten[1]=10.0**daten[1]*8.617328149e-5 # log10(K) to eV
daten[2]=daten[2]*1.e6

Arad_bg = 2.7e-31*1.e6
Brad_bg = 400.
Crad_bg = 0.5

Drad_bg = 2.82e-31*1.e6
Erad_bg = 1555.
Frad_bg = 0.8

#    frad_bg     = (2./3.)*(1./(central_mass*MASS_PROTON))*((MU_ZERO*central_mass*MASS_PROTON*central_density*1.d20)**(1.5d0))                &
#                  *nimp_bg*Arad_bg*exp(-((log(T_rad)-log(Brad_bg))**2.)/Crad_bg**2.)                                                         &
#                + (2./3.)*(1./(central_mass*MASS_PROTON))*((MU_ZERO*central_mass*MASS_PROTON*central_density*1.d20)**(1.5d0))                &
#                  *nimp_bg*Drad_bg*exp(-((log(T_rad)-log(Erad_bg))**2.)/Grad_bg**2.)                                                         

def frad_bg(T_rad):
  return                Arad_bg*np.exp(-((np.log(T_rad)-np.log(Brad_bg))**2.)/Crad_bg**2.) + Drad_bg*np.exp(-((np.log(T_rad)-np.log(Erad_bg))**2.)/Frad_bg**2.)


'''
figsize=(9.,8.)

fig = plt.figure(figsize=figsize)
plt.xlabel("T/eV")
plt.ylabel("Prad/(Wcm-3)")
plt.grid(zorder=0)
sns.despine(offset=10)
plt.xlim([1,10000])
plt.ylim([1.e-38,2.e-24])

plt.loglog(daten[1],daten[2],label="0D-Modell (ADAS w_89)")
plt.loglog(daten[1],frad_bg(daten[1]),label="frad_bg")
plt.legend()
plt.savefig("Prad-W-Comp.png") 

'''
figsize=(9.,8.)

fig = plt.figure(figsize=figsize)
plt.xlabel("T/eV")
plt.ylabel("Prad/(Wcm-3)")
plt.grid(zorder=0)
sns.despine(offset=10)
plt.xlim([1,5000])


plt.plot(daten[1],daten[2],label="0D-Modell (ADAS w_89)")
plt.plot(daten[1],frad_bg(daten[1]),label="frad_bg")
plt.legend()
#plt.show()
plt.savefig("Prad-w.png") 

