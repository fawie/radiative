# -*- coding: utf-8 -*-

exec(open("Main.py").read())
exec(open("Standardfunctions.py").read())
exec(open("Poincare.py").read())
#exec(open("Postproc.py").read())
exec(open("Four.py").read())
exec(open("Coils.py").read())
from scipy.optimize import curve_fit

print("data=InitRunFolder(Postproc=False,Poincare=False,Four=False,Energies=False,Growth_Rates=False,Diag_coils=False,Other=False)")

print("all_graphs(dEnergies=True,dTimes=True,dRadius=True,dFitting_R=True,dPhi=True,dFitting_phi=True,dPotentials=True,dPoincare=True)")

print("will save in plots")


HFS="HFS"
LFS="LFS"

saddletemp=["$0<\phi<\pi/2$",
            "$\pi/2<\phi<\pi$",
            "$\pi<\phi<\pi3/4$",
            "$\pi3/4<\phi<\pi2$"]

SaddleInfo={"label1":[HFS for i in range(40)]+[LFS for i in range(40)],
            "label2":20*saddletemp,
            "phi":20*[0,np.pi/2,np.pi,np.pi*3/2],
            "R":np.array([1.0845-0.04*(np.floor(i/4)) for i in range(40)]+[2.142+0.04*(np.floor(i/4)) for i in range(40)]),
            "Ps":[[1+j*4+i for j in range(10)] for i in range( 4)]+[[1+j*4+i+40 for j in range(10)] for i in range( 4)], #Each array contains all coils at one angle/ one side. Varying radius
            "Qs":[[1+j*4+i for i in range( 4)] for j in range(10)]+[[1+j*4+i+40 for i in range( 4)] for j in range(10)],  #Each array contains als coils at one radius/ one side. Varying angle
            "f": lambda x,A,B,C : A*x*(np.abs(x-B))**(-(C+1))
            }

mirnovtemp=["$\phi=0$",
            "$\phi=\pi/4$",
            "$\phi=\pi/2$",
            "$\phi=\pi/4*3$",
            "$\phi=\pi$",
            "$\phi=\pi/4*5$",
            "$\phi=\pi3/2$",
            "$\phi=\pi/4*7$",
            "$\phi=\pi/8$",
            "$\phi=\pi/8*3$"]

MirnovInfo={"label1":[HFS for i in range(100)]+[LFS for i in range(100)],
            "label2":20*mirnovtemp,
            "phi":20*([np.pi/4*i for i in range(8)]+[np.pi/8,np.pi/8*3]),
            "R":np.array([1.0845-0.04*(np.floor(i/10)) for i in range(100)]+[2.142+0.04*(np.floor(i/10)) for i in range(100)]),
            "Ps":[[1+j*10+i for j in range(10)] for i in range(10)]+[[1+j*10+i+100 for j in range(10)] for i in range(10)], #Each array contains all coils at one angle/ one side. Varying radius
            "Qs":[[1+j*10+i for i in [0,8,1,9,2,3,4,5,6,7]] for j in range(10)]+[[1+j*10+i+100 for i in [0,8,1,9,2,3,4,5,6,7]] for j in range(10)],  #Each array contains als coils at one radius/ one side. Varying angle
            "f": lambda x,A,B,C :A*(np.abs(x-B))**(-(C+1))
            }

CoilInfo={"Saddle":SaddleInfo,"Mirnov":MirnovInfo}

def all_graphs(dPoincare=True,dEnergies=True,dTimes=True,dRadius=True,dFitting_R=True,dPhi=True,dFitting_phi=True,dPotentials=True):


    if dPoincare:
        for step in data["run_saddle_ntor03"]['poincare/R-Z'].keys():
            Poincare_R_Z(data,"run_saddle_ntor03",step,True)

        for step in data["run_saddle_ntor03"]['poincare/rho-theta'].keys():
            Poincare_PsiN_theta(data,"run_saddle_ntor03",step,True)

    if dEnergies:
        Energies(data,["run_rad10_ntor03"],["E_{mag,00}","E_{mag,01}"],True,ylim=[1.e-29,1.],plot=True)
        Energies(data,["run_rad10_ntor05"],["E_{mag,00}","E_{mag,01}","E_{mag,02}"],True,ylim=[1.e-29,1.],plot=True)
        Energies(data,["run_rad10_ntor07"],["E_{mag,00}","E_{mag,01}","E_{mag,02}","E_{mag,03}"],True,ylim=[1.e-29,1.],plot=True)
        Energies(data,["run_rad10_ntor09"],["E_{mag,00}","E_{mag,01}","E_{mag,02}","E_{mag,03}","E_{mag,04}"],True,ylim=[1.e-29,1.],plot=True)
        Energies(data,["run_rad10_ntor11"],["E_{mag,01}","E_{mag,02}","E_{mag,03}","E_{mag,04}","E_{mag,05}"],True,ylim=[1.e-29,1.e-8],plot=True)
        Energies(data,["run_rad10_ntor11"],["E_{mag,01}","E_{mag,02}","E_{mag,03}","E_{mag,04}","E_{mag,05}"],False,ylim=[1.e-29,.2e-8],plot=True)


        data["run_rad10_ntor09"]["nmax"]=5
        Energies(data,["run_rad10_ntor03","run_rad10_ntor05","run_rad10_ntor09"],["E_{mag,01}"],True,ylim=[1.e-16,1.e-6],xlim=[0,11000],plot=True)
        Energies(data,["run_rad10_ntor03","run_rad10_ntor05","run_rad10_ntor07","run_rad10_ntor09","run_rad10_ntor11"],["E_{mag,01}"],False,ylim=[1.e-29,1.2e-7],plot=True)
        Energies(data,["run_rad10_ntor05","run_rad10_ntor07","run_rad10_ntor09","run_rad10_ntor11"],["E_{mag,00}","E_{mag,01}","E_{mag,02}"],True,ylim=[1.e-29,1],plot=True)

    if dTimes:
       Coils_Time(data,["run_saddle_ntor03"],SaddleInfo["Qs"][1],SaddleInfo,Xlim=[0,8500],HalfColor=False,plot=True)
       Coils_Time(data,["run_saddle_ntor03"],SaddleInfo["Qs"][11],SaddleInfo,Xlim=[0,8500],HalfColor=False,plot=True)
       
       Coils_Time(data,["run_saddle_ntor11"],SaddleInfo["Qs"][1],SaddleInfo,Xlim=[0,11000],Ylim=[-6.e-9,6.e-9],HalfColor=False,plot=True)
       Coils_Time(data,["run_saddle_ntor11"],SaddleInfo["Qs"][11],SaddleInfo,Xlim=[0,11000],Ylim=[-3.e-8,3.e-8],HalfColor=False,plot=True)

       Coils_Time(data,["run_saddle_ntor03","run_saddle_ntor05","run_saddle_ntor07"],[SaddleInfo["Qs"][1][0],SaddleInfo["Qs"][1][2]],SaddleInfo,Xlim=[0,8000],HalfColor=False,plot=True)

    if dRadius:
       #Coils_Radius(data,["run_rad10_ntor03"],MirnovInfo["Ps"],6000,MirnovInfo,plot=True)
       #Coils_Radius(data,["run_pol10_ntor03"],MirnovInfo["Ps"],6000,MirnovInfo,plot=True)
       Coils_Radius(data,["run_saddle_ntor03"],[SaddleInfo["Ps"][0][1:],SaddleInfo["Ps"][1][1:],SaddleInfo["Ps"][2][1:],SaddleInfo["Ps"][3][1:]],6000,SaddleInfo,plot=True)
       Coils_Radius(data,["run_saddle_ntor03"],[SaddleInfo["Ps"][4][1:],SaddleInfo["Ps"][5][1:],SaddleInfo["Ps"][6][1:],SaddleInfo["Ps"][7][1:]],6000,SaddleInfo,plot=True)

       Coils_Radius(data,["run_saddle_ntor03","run_saddle_ntor05","run_saddle_ntor07","run_saddle_ntor09","run_saddle_ntor11"],[SaddleInfo["Ps"][0][1:],SaddleInfo["Ps"][2][1:]],6000,SaddleInfo,plot=True)
       
    if dFitting_R:
       global fittingData
       f = open("../plots/fitting_R.dat","w")
       f.write('')
       f.close()
       f = open("../plots/fitting_R.dat","a")
       
       fittingData={}
       
       for run in ["run_rad10_ntor03","run_pol10_ntor03","run_rad10_ntor05","run_pol10_ntor05","run_rad10_ntor07","run_pol10_ntor07","run_rad10_ntor09","run_pol10_ntor09","run_rad10_ntor11","run_pol10_ntor11"]:
           fittingData[run]=[[],[],[],[]]
           for i in range(20):
               temp=fitting_Radius(data,run,MirnovInfo["Ps"][i][2:],MirnovInfo["Ps"][i],6000,MirnovInfo,plot=True)
               f.write(temp[0])
               fittingData[run][0].append(temp[1])
               fittingData[run][1].append(temp[2])
               fittingData[run][2].append(temp[3])
               fittingData[run][3].append(temp[4])

       for run in ["run_saddle_ntor03","run_saddle_ntor05","run_saddle_ntor07","run_saddle_ntor09","run_saddle_ntor11"]:
           fittingData[run]=[[],[],[],[]]
           for i in range(8):
               temp=fitting_Radius(data,run,SaddleInfo["Ps"][i][1:],SaddleInfo["Ps"][i],6000,SaddleInfo,plot=True)
               f.write(temp[0])
               fittingData[run][0].append(temp[1])
               fittingData[run][1].append(temp[2])
               fittingData[run][2].append(temp[3])
               fittingData[run][3].append(temp[4])

       f.close()

    if dPhi:
       Coils_Phis(data,["run_rad10_ntor03","run_pol10_ntor03"],MirnovInfo["Qs"][2],6000,MirnovInfo,plot=True)
       Coils_Phis(data,["run_rad10_ntor03","run_pol10_ntor03"],MirnovInfo["Qs"][12],6000,MirnovInfo,plot=True)
       
       Coils_Phis(data,["run_rad10_ntor11","run_pol10_ntor11"],MirnovInfo["Qs"][2],6000,MirnovInfo,plot=True)
       Coils_Phis(data,["run_rad10_ntor11","run_pol10_ntor11"],MirnovInfo["Qs"][12],6000,MirnovInfo,plot=True)
       
       Coils_Phis(data,["run_rad10_ntor03","run_rad10_ntor07","run_rad10_ntor11"],MirnovInfo["Qs"][2],6000,MirnovInfo,plot=True)
       
       Coils_Phis(data,["run_saddle_ntor03","run_saddle_ntor11"],SaddleInfo["Qs"][2],6000,SaddleInfo,plot=True)
       Coils_Phis(data,["run_saddle_ntor03","run_saddle_ntor11"],SaddleInfo["Qs"][12],6000,SaddleInfo,plot=True)
       
    if dFitting_phi:
       f = open("../plots/fitting_phi.dat","w")
       f.write('')
       f.close()
       f = open("../plots/fitting_phi.dat","a")

       f.write(fitting_Phi(data,["run_rad10_ntor03","run_pol10_ntor03"],MirnovInfo["Qs"][2],6000,MirnovInfo,plot=True))
       f.write(fitting_Phi(data,["run_rad10_ntor03","run_pol10_ntor03"],MirnovInfo["Qs"][12],6000,MirnovInfo,plot=True))
       
       f.write(fitting_Phi(data,["run_rad10_ntor07","run_pol10_ntor07"],MirnovInfo["Qs"][2],6000,MirnovInfo,plot=True))
       f.write(fitting_Phi(data,["run_rad10_ntor07","run_pol10_ntor07"],MirnovInfo["Qs"][12],6000,MirnovInfo,plot=True))

       f.write(fitting_Phi(data,["run_rad10_ntor11","run_pol10_ntor11"],MirnovInfo["Qs"][2],6000,MirnovInfo,plot=True))
       f.write(fitting_Phi(data,["run_rad10_ntor11","run_pol10_ntor11"],MirnovInfo["Qs"][12],6000,MirnovInfo,plot=True))

       for run in ["run_saddle_ntor03","run_saddle_ntor05","run_saddle_ntor09","run_saddle_ntor11"]:
           f.write(fitting_Phi(data,[run],SaddleInfo["Qs"][2],6000,SaddleInfo,plot=True))
           f.write(fitting_Phi(data,[run],SaddleInfo["Qs"][12],6000,SaddleInfo,plot=True))
       f.close()

    if dPotentials:
       #for  Xs in [MirnovInfo["Qs"][3],MirnovInfo["Qs"][13]]:
       #  for X in Xs:
          #Coils_Potential(data,"run_pol10_ntor03",X,MirnovInfo,xlim=[0,9000],ylim=[0,0.2e-7],plot=True)
          #Coils_Potential(data,"run_pol10_ntor07",X,MirnovInfo,xlim=[0,9000],ylim=[0,0.2e-7],plot=True)
          #Coils_Potential(data,"run_pol10_ntor11",X,MirnovInfo,xlim=[0,10000],ylim=[0,0.2e-7],plot=True)

          #Coils_Potential(data,"run_rad10_ntor03",X,MirnovInfo,xlim=[0,9000],ylim=[0,0.2e-7],plot=True)
          #Coils_Potential(data,"run_rad10_ntor07",X,MirnovInfo,xlim=[0,9000],ylim=[0,0.2e-7],plot=True)
          #Coils_Potential(data,"run_rad10_ntor11",X,MirnovInfo,xlim=[0,10000],ylim=[0,0.2e-7],plot=True)

       for  Xs in [SaddleInfo["Qs"][3],SaddleInfo["Qs"][13]]:
         for X in Xs:
          #Coils_Potential(data,"run_saddle_ntor03",X,SaddleInfo,xlim=[0,9000],ylim=[0,0.2e-7],plot=True)
          #Coils_Potential(data,"run_saddle_ntor07",X,SaddleInfo,xlim=[0,9000],ylim=[0,0.2e-7],plot=True)
          Coils_Potential(data,"run_saddle_ntor11",X,SaddleInfo,xlim=[0,10000],ylim=[0,0.2e-7],plot=True)

