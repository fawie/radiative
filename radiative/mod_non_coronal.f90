!> module takes the OPEN-ADAS data to calculate the non_coronal equil-
!> ibrium temperature and radiation.
!> If you need time-dependent solutions of the corona matrix timestepping look
!> in the revision history for this file.
module mod_non_coronal
use mod_openadas
use mod_interp_splinear
use constants
implicit none

public non_coronal

! -------------------------------------------------------------------------------------
! --- Definition of the datatype
! -------------------------------------------------------------------------------------
type non_coronal
  type (ADF11_all)               :: ad                !< ADF11 datatype

  !Simulation variables
  real*8                         :: time                  !< current time
  real*8                         :: temperature           !< current electron temperature
  real*8                         :: density               !< current electrion density
  real*8                         :: density_plasma        !< constant density of ions of the background plasma
  real*8                         :: density_imp           !< constant density of the imuprity ions
  real*8, dimension(3)           :: C_cooling             !< Coefficient for linear aritifical cooling C(1): dT/dt C(2): when starts cooling, C(3): Tmin
  real*8                         :: t_rad, t_equi, t_pres !< when starts radiative cooling, when go into coronal equilbrium, when starts adiabatic
  real*8, allocatable            :: fractions(:)          !< Fractional charge states of impurites

  ! Constants
  integer                        :: n_Z0              !< Z plus 1 to factor in the atomic state
  real*8, allocatable            :: Z(:)              !< The charge number at each charge state
  real*8, allocatable            :: ZZ(:)             !< The squared charge number at each charge state

  ! Temporary auxiliary values
  real*8, allocatable            :: ion_rate(:)       !< Current ionization rates
  real*8, allocatable            :: rec_rate(:)       !< Current recombinations rates
  real*8, allocatable            :: rad(:)            !< Current radiation coefficients
  real*8, allocatable            :: rad_LT(:)            !< Current radiation coefficients
  real*8, allocatable            :: rad_RB(:)            !< Current radiation coefficients

contains
  procedure :: set_T             => set_T
  procedure :: set_N             => set_N
  procedure :: set_equilibrium   => set_equilibrium
  procedure :: update            => update_main
  procedure :: output            => output_main
  procedure :: output_header     => output_header

  procedure :: calculate_GCRs    => calculate_GCRs
  procedure :: Prad              => calculate_Prad
  procedure :: Lrad              => calculate_Lrad
  procedure :: z_EFF             => calculate_z_EFF
  procedure :: z_avg             => calculate_z_avg
  procedure :: equilibrium       => calculate_equilibrium
  procedure :: cooling           => calculate_cooling
end type non_coronal
interface non_coronal
  module procedure non_coronal_equilibrium
end interface non_coronal
contains



! -------------------------------------------------------------------------------------
! --- Setup the datatype
! -------------------------------------------------------------------------------------
function non_coronal_equilibrium(ad,temperature,density_plasma,density_imp,t_rad,t_equi,t_pres,C_cooling) result(cor)
use constants
type (ADF11_all),     intent(in) :: ad                    !< ADF11 datatype
real*8,               intent(in) :: density_plasma        !< plasma (ion) density
real*8,               intent(in) :: density_imp           !< impurity (ion) density
real*8,               intent(in) :: temperature           !< initial temperature
real*8,               intent(in) :: t_rad, t_equi, t_pres !< when starts radiative cooling, when go into coronal equilbrium
real*8, dimension(3), intent(in) :: C_cooling             !< Coefficient for linear aritifical cooling C(1) dT/dt C(2) when starts cooling, C(3) Tmin
!real*8,              intent(in) :: tau_diff              !< Characteristic Diffusion time
type (non_coronal)               :: cor                   !< non_coronal equilibrium datatype
integer                          :: iz

cor%ad           = ad
cor%n_Z0         = ad%n_Z+1

allocate(cor%fractions(cor%n_Z0),cor%Z(cor%n_Z0),cor%ZZ(cor%n_Z0),cor%ion_rate(ad%n_Z),cor%rec_rate(ad%n_Z),cor%rad(ad%n_Z),cor%rad_LT(ad%n_Z),cor%rad_RB(ad%n_Z))

do iz=1,cor%n_Z0
  cor%Z(iz)      = real(iz-1,8)
  cor%ZZ(iz)      = real(iz-1,8)**2
enddo

cor%time           = 0.d0
cor%temperature    = temperature
cor%density        = density_plasma
cor%density_plasma = density_plasma
cor%density_imp    = density_imp
cor%fractions      = 0.0d0
cor%fractions(1)   = 1.0d0
cor%C_cooling      = C_cooling
cor%t_rad          = t_rad
cor%t_equi         = t_equi
cor%t_pres         = t_pres

call cor%calculate_GCRs()

end function non_coronal_equilibrium




! -------------------------------------------------------------------------------------
! -------------------------------------------------------------------------------------
! ------------------------------- SET VALUES ------------------------------------------
! -------------------------------------------------------------------------------------
! -------------------------------------------------------------------------------------



! -------------------------------------------------------------------------------------
! --- Set Temperature
! -------------------------------------------------------------------------------------
subroutine set_T(cor,temperature)
class(non_coronal),  intent(inout) :: cor !< Coronal equilibrium type
real*8, intent(in)                 :: temperature !< log10 temperature in m^-3

cor%temperature=temperature
call cor%calculate_GCRs()

end subroutine set_T



! -------------------------------------------------------------------------------------
! --- Set Density
! -------------------------------------------------------------------------------------
subroutine set_N(cor,density)
class(non_coronal),  intent(inout) :: cor !< Coronal equilibrium type
real*8, intent(in)                 :: density !< log10 density in m^-3

cor%density=density
call cor%calculate_GCRs()

end subroutine set_N


! -------------------------------------------------------------------------------------
! --- Set fractions to equilibrium
! -------------------------------------------------------------------------------------
subroutine set_equilibrium(cor)
class(non_coronal),  intent(inout) :: cor !< Coronal equilibrium type

cor%fractions = cor%equilibrium()

end subroutine set_equilibrium


! -------------------------------------------------------------------------------------
! -------------------------------------------------------------------------------------
! ------------------------------- MAIN ROUTINES ---------------------------------------
! -------------------------------------------------------------------------------------
! -------------------------------------------------------------------------------------



! -------------------------------------------------------------------------------------
! --- Updates f temporal evolution
! -------------------------------------------------------------------------------------
subroutine update_main(cor,dtime)

class(non_coronal), intent(inout)  :: cor
real*8, intent(in)                 :: dtime
real*8, dimension(cor%n_Z0)        :: fractions0, dfractions_dt
real*8                             :: density0,temperature0, ddensity_dt, dtemperature_dt, dnT_rad_dt, density_tot0, dT_cool_dt
integer                            :: iz

dnT_rad_dt          = cor%Prad()
dT_cool_dt          = cor%cooling()

density0           = cor%density
temperature0       = cor%temperature
fractions0         = cor%fractions
density_tot0       = cor%density_plasma + cor%density_imp + cor%density

dtemperature_dt    = 0.d0
ddensity_dt        = 0.d0
dfractions_dt      = 0.d0

! ---- non coronal
if ( cor%time .lt. cor%t_equi ) then
  do iz=1,cor%n_Z0
    if ( iz .gt. 1) then
      dfractions_dt(iz) = dfractions_dt(iz)                                                  &
                          + cor%fractions(iz-1) * cor%density * cor%ion_rate(iz-1)   & ! Gain by Ionization    from (iz-1) to iz
                          - cor%fractions(iz)   * cor%density * cor%rec_rate(iz-1)     ! Loss by Recombination from iz     to (iz-1)
    end if
    if ( iz .lt. cor%n_Z0) then
      dfractions_dt(iz) = dfractions_dt(iz)                                                  &
                          + cor%fractions(iz+1) * cor%density * cor%rec_rate(iz)     & ! Gain by Recombination from (iz+1) to iz
                          - cor%fractions(iz)   * cor%density * cor%ion_rate(iz)       ! Loss by Ionization    from iz     to (iz+1)
     end if
  end do

  cor%fractions     = fractions0   + dfractions_dt *dtime
  ddensity_dt       = cor%density_imp * dot_product(dfractions_dt,cor%Z)


! --- coronal equilibrium
else
  cor%fractions = cor%equilibrium()
  ddensity_dt   = cor%density_imp * ( dot_product(cor%fractions,cor%Z) - dot_product(fractions0,cor%Z) ) / dtime
end if

if ( cor%time .lt. cor%t_pres ) then
  ddensity_dt = 0.d0
end if

!Diffusion and fueling
!dfractions_dt    = cor%fractions/tau
!dfractions_dt(1) = sum(cor%dfractions_dt(2:cor%n_Z0))

if ( cor%time .gt. cor%t_rad) then
  dtemperature_dt   = - 2.d0 / 3.d0 *   dnT_rad_dt / density_tot0
end if
  dtemperature_dt   = dtemperature_dt  &
                      - dT_cool_dt     &
                      - ddensity_dt * temperature0 / density_tot0

cor%density       = density0     + ddensity_dt * dtime
cor%time          = cor%time     + dtime
cor%temperature   = temperature0 + dtemperature_dt * dtime

do iz=1,cor%n_Z0
  if ( cor%fractions(iz) .lt. 1.d-99 ) then
    cor%fractions(iz) = 0.d0
  end if
end do

call cor%calculate_GCRs()

end subroutine update_main

! ----------------------------------------------------------------------------------
! --- Print header
! ----------------------------------------------------------------------------------
subroutine output_header(cor,pointer,output_commands)
  class(non_coronal), intent(in)                       :: cor !< Coronal equilibrium type
  integer, intent(in)                                  :: pointer
  character(len=16), dimension(OUTPUT_MAX), intent(in) :: output_commands
  integer                                              :: iz, c
  character(len=21)                                    :: header_Z

  write(pointer,'(A)',advance='no') '# '

  do c=1,OUTPUT_MAX
    select case( output_commands(c) )
      case ('t')
        write(pointer,'(A21,A2)',advance='no')   'time','  '
      case ('Te')
        write(pointer,'(A21,A2)',advance='no')   'temperature','  '
      case ('ne')
        write(pointer,'(A23)',advance='no')   'density  '
      case ('fractions')
         do iz = 1,  cor%n_Z0
           write(header_Z,'(A10,I2,A1)')  'fractions(',iz,')  '
           write(pointer,'(A23)',advance='no') header_Z
         end do
      case ('ion_rate')
         do iz = 1,  cor%ad%n_Z
           write(header_Z,'(A10,I2,A1)')  'ion_rate(',iz,')  '
           write(pointer,'(A23)',advance='no') header_Z
         end do
      case ('rec_rate')
         do iz = 1,  cor%ad%n_Z
           write(header_Z,'(A10,I2,A1)')  'rec_rate(',iz,')  '
           write(pointer,'(A23)',advance='no') header_Z
         end do
      case ('Z_eff')
        write(pointer,'(A23)',advance='no')   'Z_eff  '
      case ('<Z>')
        write(pointer,'(A23)',advance='no')   '<Z>  '
      case ('P_rad')
        write(pointer,'(A23)',advance='no')   'P_rad  '
      case ('L_rad')
        write(pointer,'(A23)',advance='no')   'L_rad  '
      case ('log10_Te')
        write(pointer,'(A23)',advance='no')   'log10_Te  '
      case ('log10_ne')
        write(pointer,'(A23)',advance='no')   'log10_ne  '
    end select
  end do

  ! --- new line
  write(pointer,'(a)') '  '

end subroutine output_header


! ----------------------------------------------------------------------------------
! --- Print data out for a time step
! ----------------------------------------------------------------------------------
subroutine output_main(cor,pointer,output_commands,unit)
  class(non_coronal), intent(in)                       :: cor !< Coronal equilibrium type
  integer, intent(in)                                  :: pointer
  character(len=16), dimension(OUTPUT_MAX), intent(in) :: output_commands
  integer, intent(in)                                  :: unit
  integer                                              :: iz, c
  real*8                                               :: z_avg, z_EFF, P_rad, log10_Te, log10_ne, L_rad
  real*8                                               :: norm_Th, norm_T, norm_M, norm_N, norm_E

  if ( unit .lt. 1 ) then
    norm_Th = 1.d0
    norm_T  = 1.d0
    norm_N  = 1.d0
    norm_M  = 1.d0
    norm_E  = 1.d0
  else
    if ( unit .eq. 1 ) then
      norm_Th = 1.d0/(EL_CHG*MU_ZERO*1.d20)
    else
      norm_Th = 1.d0/(K_BOLTZ*MU_ZERO*1.d20)
    end if
    norm_T  = sqrt_mu0_rho0
    norm_N  = 1.d20
    norm_M  = 1.d20*MASS_PROTON
    norm_E  = 1.d0/MU_ZERO
  end if

  write(*,*) "time:",cor%time

  do c=1,OUTPUT_MAX
    select case( output_commands(c) )
      case ('t')
        write(pointer,'(a,ES21.15)',advance='no')   '  ',cor%time * norm_T
      case ('Te')
        write(pointer,'(a,ES21.15)',advance='no')   '  ',cor%temperature * norm_Th
      case ('ne')
        write(pointer,'(a,ES21.15)',advance='no')   '  ',cor%density * norm_N
      case ('fractions')
         do iz = 1,  cor%n_Z0
           write(pointer,'(a,ES21.15)',advance='no') '  ',cor%fractions(iz) 
         end do
      case ('ion_rate')
         do iz = 1,  cor%ad%n_Z
           write(pointer,'(a,ES21.15)',advance='no') '  ',cor%ion_rate(iz) / norm_T / norm_N
         end do
      case ('rec_rate')
         do iz = 1,  cor%ad%n_Z
           write(pointer,'(a,ES21.15)',advance='no') '  ',cor%rec_rate(iz) / norm_T / norm_N
         end do
      case ('Z_eff')
        z_EFF= cor%z_EFF()
        write(pointer,'(a,ES21.15)',advance='no')   '  ',z_EFF
      case ('P_rad')
        P_rad= cor%Prad()
        write(pointer,'(a,ES21.15)',advance='no')   '  ',P_rad * norm_E / norm_T
      case ('L_rad')
        L_rad= cor%Lrad()
        write(pointer,'(a,ES21.15)',advance='no')   '  ',L_rad * norm_E / norm_T / norm_N**2
      case ('<Z>')
        z_avg= cor%z_avg()
        write(pointer,'(a,ES21.15)',advance='no')   '  ',z_avg
      case ('log10_Te')
        log10_Te = log10(cor%temperature/(K_BOLTZ*MU_ZERO*1.d20))
        write(pointer,'(a,ES21.15)',advance='no')   '  ',log10_Te
      case ('log10_ne')
        log10_ne = log10(cor%density*1.d20)
        write(pointer,'(a,ES21.15)',advance='no')   '  ',log10_ne
    end select
  end do



  ! --- new line
  write(pointer,'(a)') '  '

end subroutine output_main




! -------------------------------------------------------------------------------------
! -------------------------------------------------------------------------------------
! ------------------------------- CALCULATIONS ----------------------------------------
! -------------------------------------------------------------------------------------
! -------------------------------------------------------------------------------------



! -------------------------------------------------------------------------------------
! --- Caclculate Coefficients
! -------------------------------------------------------------------------------------
subroutine calculate_GCRs(cor)
class(non_coronal),  intent(inout) :: cor 
integer                            :: iz
real*8                             :: rad_RB, rad_LT, log10_Te, log10_ne

log10_Te = log10(cor%temperature/(K_BOLTZ*MU_ZERO*1.d20))
log10_ne = log10(cor%density*1.d20)

do iz=1,cor%ad%n_Z
  call cor%ad%ACD%interp(iz, log10_ne, log10_Te, cor%rec_rate(iz))
  call cor%ad%SCD%interp(iz, log10_ne, log10_Te, cor%ion_rate(iz))
  call cor%ad%PRB%interp(iz, log10_ne, log10_Te, rad_RB)
  call cor%ad%PLT%interp(iz, log10_ne, log10_Te, rad_LT)
  cor%rad_RB(iz) = rad_RB ; cor%rad_LT(iz) = rad_LT
  cor%rad(iz) = rad_RB + rad_LT
end do

!  Convert to JOREK units
cor%rec_rate = cor%rec_rate * sqrt_mu0_rho0 * 1.d20
cor%ion_rate = cor%ion_rate * sqrt_mu0_rho0 * 1.d20
cor%rad      = cor%rad   * MU_ZERO * sqrt_mu0_rho0 *  1.d40
cor%rad_LT   = cor%rad_LT   * MU_ZERO * sqrt_mu0_rho0 *  1.d40
cor%rad_RB   = cor%rad_RB   * MU_ZERO * sqrt_mu0_rho0 *  1.d40

end subroutine calculate_GCRs



! -------------------------------------------------------------------------------------
! --- Calculates Cooling source
! -------------------------------------------------------------------------------------
function calculate_cooling(cor)
class(non_coronal), intent(in) :: cor !< Coronal equilibrium type
real*8                         :: calculate_cooling

if ( cor%time .gt. cor%C_cooling(2) .and. cor%temperature .gt. cor%C_cooling(3) ) then
  calculate_cooling = cor%C_cooling(1)
else
  calculate_cooling = 0.d0
end if

end function calculate_cooling



! -------------------------------------------------------------------------------------
! --- Calculates Coronal equilibrium
! -------------------------------------------------------------------------------------
function calculate_equilibrium(cor)
class(non_coronal), intent(in)     :: cor !< Coronal equilibrium type
real*8, dimension(cor%n_Z0)        :: calculate_equilibrium
integer                            :: iz

calculate_equilibrium    = 0.d0
calculate_equilibrium(1) = 1.0d0

do iz=2,cor%n_Z0
  calculate_equilibrium(iz) = calculate_equilibrium(iz-1) * cor%ion_rate(iz-1)/cor%rec_rate(iz-1)
end do

calculate_equilibrium = calculate_equilibrium / sum(calculate_equilibrium)

end function calculate_equilibrium



! -------------------------------------------------------------------------------------
! --- Calculates P_rad
! -------------------------------------------------------------------------------------
function calculate_Prad(cor)
class(non_coronal), intent(in) :: cor 
real*8                         :: calculate_Prad !< Output power in W m-3

!calculate_Prad = cor%density*cor%density_imp*dot_product(cor%fractions(2:cor%n_Z0), cor%rad)
calculate_Prad = cor%density*cor%density_imp*(dot_product(cor%fractions(1:cor%ad%n_Z), cor%rad_LT) + dot_product(cor%fractions(2:cor%n_Z0), cor%rad_RB))


end function calculate_Prad

! -------------------------------------------------------------------------------------
! --- Calculates L_rad
! -------------------------------------------------------------------------------------
function calculate_Lrad(cor)
class(non_coronal), intent(in) :: cor 
real*8                         :: calculate_Lrad !< Output power in W m3

calculate_Lrad = dot_product(cor%fractions(1:cor%ad%n_Z), cor%rad_LT ) + dot_product(cor%fractions(2:cor%n_Z0), cor%rad_RB  )

end function calculate_Lrad

! -------------------------------------------------------------------------------------
! --- Calculates average charge state for given f
! -------------------------------------------------------------------------------------
function calculate_z_avg(cor)
class(non_coronal), intent(in) :: cor !< Coronal equilibrium type
real*8                         :: calculate_z_avg

calculate_z_avg = dot_product(cor%fractions,cor%Z)

end function calculate_z_avg


! -------------------------------------------------------------------------------------
! --- Calculates z_EFF for given f
! -------------------------------------------------------------------------------------
function calculate_z_EFF(cor)
class(non_coronal), intent(in) :: cor !< Coronal equilibrium type
real*8                         :: calculate_z_EFF, down

calculate_z_EFF = dot_product(cor%fractions,cor%ZZ)
down            = dot_product(cor%fractions,cor%Z)

if (down .gt. 0.d0 ) calculate_z_EFF = calculate_z_EFF/down

end function calculate_z_EFF



end module mod_non_coronal
