program Radiative

  use mod_openadas
  use mod_non_coronal
  implicit none

  type(adf11_all)                  :: adas
  type(non_coronal)                :: ncor
  character(len=128)               :: outputname
  character(len=5)                 :: ADASname
  real*8                           :: tstep, ftime, temperature, density_plasma, density_imp, t_rad, t_equi, t_pres
  real*8, dimension(3)             :: C_cooling
  integer                          :: step, nout, ierr, unit
  logical                          :: line_zero
  character(len=16), dimension(OUTPUT_MAX) :: output_commands

  namelist /input/ outputname, output_commands, ADASname, &
                 tstep, ftime, temperature, density_plasma, density_imp, t_rad, t_equi, &
                 C_cooling, nout, line_zero, t_pres, unit


! --- Prepare the evolution
write(*,*) '#### Prepare Evolution #####'
read(5,input)
adas     = read_adf11(ADASname)                                                                    ! ADAS Data for choosen Impurity
ncor     = non_coronal(adas,temperature,density_plasma,density_imp,t_rad,t_equi,t_pres,C_cooling)  ! Non-Coronal-Type



! --- Output first line for stime = 0
open(20,file=outputname)
call ncor%output_header(20,output_commands)
if ( line_zero .eq. .true. ) then
  call ncor%output(20,output_commands,unit) 
end if



! --- Start the Evolution
write(*,*) '##### Start Evolution #####'
write(*,*) '##### #Steps:',int(ftime/tstep),'####'
write(*,*) '##### #Lines:',int(ftime/tstep)/nout,'###'


do step=1,int(ftime/tstep)
  ! --- Time step
  call ncor%update(tstep)
  ! --- Output
  if ( int(mod(step,nout)) .eq. 0 )  call ncor%output(20,output_commands,unit)
end do



close (20)
end program Radiative
