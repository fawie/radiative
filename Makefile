# Makefile for compiling fortran programs with automatic dependency
# generation and recognition of programs. Full support for parallel make.
#
# Object files are stored in $(OBJDIR)=.obj/
# Module files are stored in $(MODDIR)=.mod/
# Dependencies are stored in $(DEPDIR)=.dep/
#
# Known limitations:
# - Module/function/subroutine name must be equal to file name

# Include jorek-specific things and settings
include Makefile.inc
# Default rule needs to be first
main: jorek2_radiative

# Some defaults and parsing logic for makefile.inc
include defaults.mk

# Build the main executable with a different name from the source file jorek2_radiative.o
jorek2_radiative: $(OBJDIR)/jorek2_radiative.o $(shell ./util/obj_deps $(DEPDIR)/jorek2_radiative.d)
	$(FC) $(FLAGS) $(DEFINES) $(INCLUDES) -o $@ $^ $(LIBS)


.PHONY: clean cleanall cleandep duplicates
cleanall: clean cleandep
clean:
	@echo ">> Deleting Object Files <<"
	-@rm -r $(OBJDIR)
	@echo ">> Deleting Module Files <<"
	-@rm -r $(MODDIR)
	-@find . -name '*.mod' -delete -or -name '*.o' -delete
cleandep:
	@echo ">> Deleting Dependency Files <<"
	-@rm -r $(DEPDIR)
	-@find . -name '*.d' -delete 2>/dev/null
doc docs:
	-@rm -r doc/ # workaround for FORD bug
	ford jorek.md --no-search $(INCLUDES)



# Directories containing sources, ordered by number of files
DIRS := tools				\
	.				\
	radiative
DIRS+=$(EXTRA_DIRS) # Specified in Makefile.inc or commandline

# All .f90 files we should generate .d dependency files for
depends:=$(basename $(notdir $(shell find $(DIRS) -maxdepth 1 -iname '*.f90')))
depends:=$(foreach dep,$(depends),$(DEPDIR)/$(dep).d)

# Check for multiple files with the same name in $(DIRS)
duplicates:
	@dups=`find $(DIRS) -maxdepth 1 -iname '*.f90' |\
	  xargs basename -s .f90 -a | sort | uniq -d`
	@if [[ -z "${dups// }" ]]; then echo "No duplicates detected"; \
	else echo "Duplicates found: ${dups}"; fi

printsettings:
	@echo "FC        = $(FC)"
	@echo "CC        = $(CC)"
	@echo "DEBUG     = $(DEBUG)"
	@echo "FLAGS     = $(FLAGS)"
	@echo "FFLAGS    = $(FFLAGS)"
	@echo "F90FLAGS  = $(F90FLAGS)"
	@echo "F77FLAGS  = $(F77FLAGS)"
	@echo "CFLAGS    = $(CFLAGS)"
	@echo "DEFINES   = $(DEFINES)"
	@echo "INCLUDES  = $(INCLUDES)"
	@echo "LIBS      = $(LIBS)"

# For each source dir add an explicit rule with the template
$(foreach dir,$(DIRS),$(eval $(call O_TEMPLATE,$(dir)/)))
# For each source dir add a rule to create dependency files
ifneq (cleanall,$(findstring cleanall,$(MAKECMDGOALS)))
  ifneq (cleandep,$(findstring cleandep,$(MAKECMDGOALS)))
    $(foreach dir,$(DIRS),$(eval $(call F90_D_TEMPLATE,$(dir)/)))
  endif
endif
# Include the dependency files that can be generated by the rules above
-include $(depends) # Make will build all of these and re-execute

# The included programs are defined in the $(depends) files
# For each of these programs add the default template
$(foreach prog,$(PROGRAM_SOURCES),$(eval $(call PROGRAM_TEMPLATE,$(prog))))
# To compile all programs found in $(DIRS)
all: $(basename $(notdir $(PROGRAM_SOURCES)))
# A list of supported diagnostics, that should compile properly for regression
# testing to pass
most: jorek2_radiative
# Make all object files we know of
find_files = $(wildcard $(dir)/*.f90) $(wildcard $(dir)/*.c) $(wildcard $(dir)/*.f) $(wildcard $(dir)/*.cpp)
objs: $(foreach file,$(foreach dir,$(DIRS), $(find_files)), $(OBJDIR)/$(notdir $(basename $(file))).o)

# Special cases
# Add here: Global includes (as the line below)
INCLUDES += -Itools # for r3_info.h
INCLUDES += -Imodels
# C++ support
LIBS += -lstdc++
CXXFLAGS += -pedantic -Wall


