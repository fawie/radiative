# JOREK configuration file for compilation on the DRACO cluster at IPP Garching
# see also https://www.jorek.eu/wiki/doku.php?id=draco
 
 
FC                = mpiifort  -mcmodel=large
CC                = mpiicc   
CXX               = mpiicpc  
 
USE_PASTIX        = 1
 
# --- PaStiX solver
PASTIX_HOME = /u/mhoelzl/LIBS/2018/pastix_release_4492
LIB_PASTIX        = `$(PASTIX_HOME)/install/pastix-conf --libs` 
LIB_PASTIX_MURGE  = `$(PASTIX_HOME)/install/pastix-conf --libs_murge`
LIB_PASTIX_BLAS   = `$(PASTIX_HOME)/install/pastix-conf --blas`
INC_PASTIX        = `$(PASTIX_HOME)/install/pastix-conf --incs`
